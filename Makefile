REG:=registry.gitlab.com/kanedak
APP:=hello-aleph-kn
TAG:=latest

image: dist
	pack build $(REG)/$(APP) --buildpack gcr.io/paketo-buildpacks/nginx --publish

dist:
	aleph build

run: pull
	docker run --name $(APP) --rm --tty --env PORT=8080 --publish 8080:8080 $(REG)/$(APP)

pull:
	docker pull $(REG)/$(APP)

create: # to deploy at first
	kn service create  $(APP) --image $(REG)/$(APP)

deploy:
	kn service update $(APP) --image $(REG)/$(APP)

clean:
	$(RM) -rf dist
	-@docker rm $(APP) 2>/dev/null
	-@docker rmi $(APP) 2>/dev/null
